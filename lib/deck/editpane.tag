<wb-editpane>
    <style>
        :scope {
            text-align: center;
        }

        .editpane-centered {
            padding: 20vh 20vw;
        }
        img {
            width: 80px;
            height: 80px;
        }
        x-box {
            height: 100px;
            width: 100px;
        }
    </style>
    <div class="editpane-centered">
        <!--
        <x-card>
            <x-card each={opts.types}>
                <x-box vertical>
                    <raw html={preview}></raw>
                    <x-label>{label}</x-label>
                </x-box>
            </x-card>
        </x-card>
        -->
    </div>

    <script>
    </script>
</wb-editpane>
