<wb-title>
    <style scoped>
        h1 {
            font-size: 7vh;
            text-align: center;
        }
        .pane-fullscreen h1 {
            margin-top: 45vh;
            font-size: 10vh;
        }
    </style>
    <h1>{opts.text}</h1>
</wb-title>
